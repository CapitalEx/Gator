extends Node

func eval(word: String, stack: Array) -> void:
	match word:
		"swap":
			var b = stack.pop_back()
			var a = stack.pop_back()
			stack.push_back(b)
			stack.push_back(a)

		"get-tree":
			stack.push_back(get_tree())

		"new":
			var a = stack.pop_back()
			stack.push_back(a.new())

		"drop": 
			stack.pop_back()

		"dup":
			var a = stack.pop_back()
			stack.push_back(a)
			stack.push_back(a)

		"at", "nth": 
			var b = stack.pop_back()
			var a = stack.pop_back()
			stack.push_back(a[b])

		"is": 
			var b = stack.pop_back()
			var a = stack.pop_back()
			stack.push_back(a is b)

		"~": 
			var a = stack.pop_back()
			stack.push_back(~a)

		"neg": 
			var a = stack.pop_back()
			stack.push_back(-a)

		"*": 
			var b = stack.pop_back()
			var a = stack.pop_back()
			stack.push_back(a * b)

		"/": 
			var b = stack.pop_back()
			var a = stack.pop_back()
			stack.push_back(a / b)
		"%": 
			var b = stack.pop_back()
			var a = stack.pop_back()
			stack.push_back(a % b)
		"+":
			var b = stack.pop_back()
			var a = stack.pop_back()
			stack.push_back(a + b)

		"-": 
			var b = stack.pop_back()
			var a = stack.pop_back()
			stack.push_back(a - b)

		"<<": pass
		">>": pass
		"&": pass
		"^": pass
		"|": pass
		"<": 
			var b = stack.pop_back()
			var a = stack.pop_back()
			stack.push_back(a < b)
		">": 
			var b = stack.pop_back()
			var a = stack.pop_back()
			stack.push_back(a > b)
		"==": 
			var b = stack.pop_back()
			var a = stack.pop_back()
			stack.push_back(a == b)
		"!=": 
			var b = stack.pop_back()
			var a = stack.pop_back()
			stack.push_back(a != b)
			
		">=": pass
		"<=": pass
		"in": pass
		"!", "not": pass
		"&&", "and": pass
		"||", "or": pass
		"if": pass
		"as": pass
		"$": pass
		"and": pass
		"Color8": pass
		"ColorN": pass
		"abs": pass
		"acos": pass
		"asin": pass
		"assert": pass
		"atan": pass
		"atan2": pass
		"bytes2var": pass
		"cartesian2polar": pass
		"ceil": pass
		"char": pass
		"clamp": pass
		"convert": pass
		"cos": pass
		"cosh": pass
		"db2linear": pass
		"decimals": pass
		"dectime": pass
		"deg2rad": pass
		"dict2inst": pass
		"ease": pass
		"exp": pass
		"floor": pass
		"fmod": pass
		"fposmod": pass
		"funcref": pass
		"hash": pass
		"inst2dict": pass
		"instance_from_id": pass
		"inverse_lerp": pass
		"is_inf": pass
		"is_nan": pass
		"len": pass
		"lerp": pass
		"linear2db": pass
		"load": pass
		"log": pass
		"max": pass
		"min": pass
		"nearest_po2": pass
		"parse_json": pass
		"polar2cartesian": pass
		"pow": pass
		"preload": pass
		"print": 
			var a = stack.pop_back()
			print(a)

		"print_stack": 
			print_stack()

		"printerr": pass
		"printraw": pass
		"prints": pass
		"printt": pass
		"rad2deg": pass
		"rand_range": pass
		"rand_seed": pass
		"randf": pass
		"randi": pass
		"randomize": pass
		"range": pass
		"range_lerp": pass
		"round": pass
		"seed": pass
		"sign": pass
		"sin": 
			var x = stack.pop_back()
			stack.push_back(sin(x))

		"sinh": 
			var x = stack.pop_back()
			stack.push_back(sinh(x))

		"sqrt": 
			var x = stack.pop_back()
			stack.push_back(sqrt(x))

		"stepify": pass
		"str": pass
		"str2var": pass
		"tan": pass
		"tanh": pass
		"to_json": pass
		"type_exists": pass
		"typeof": pass
		"validate_json": pass
		"var2bytes": pass
		"var2str": pass
		"weakref": pass
		"wrapf": pass
		"wrapi": pass