class_name StackMachine
extends Node
signal stack_changed(stack)

enum {
	ERROR_OK,
	ERROR_EOF,
	ERROR_UNDEFINED_WORD,
	ERROR_STACK_UNDER_FLOW,
	ERROR_TYPE_ERROR,
}


class DefineWordParseResult:
	extends Reference
	var status: int
	var body: Array
	var i: int

	func _init(_status: int, _body: Array, _i: int):
		status = _status
		body = _body
		i = _i


class DefineArrayParseResult:
	pass



class DefineDictParseResult:
	pass



class Word:
	extends Reference
	var name: String

	func _init(_name: String) -> void:
		name = _name

	func _to_string() -> String:
		return "Word{ name: %s }" % name
	

class BoxedString:
	extends Reference
	var value: String
	
	func _init(_value: String) -> void:
		value = _value
	
	func _to_string() -> String:
		return "BoxedString{ value: %s }" % value


var stack = []
var dictionary = {}
var float_regex = RegEx.new()
var int_regex = RegEx.new()


func _ready() -> void:
	float_regex.compile("^(?:[0-9]+.[0-9]+|[0-9]+.|.[0-9]+)$")
	int_regex.compile("^[0-9]+$")


func tokenize(program: String) -> Array:
	var current_token := PoolStringArray([])
	var token_list := []
	var in_string := false
	var should_push_token := false

	# Collect all the tokens in the program
	# Each token is either separated by a space
	# or is wraped in double quotes.
	for chr in program:
#		printt("BEFORE:", chr, in_string, should_push_token)
		if chr == '"':
			if in_string: 
				should_push_token = true 
			else:
				in_string = true 

		elif not chr in " \n\r\t" or in_string:
			current_token.push_back(chr)

		else:
			should_push_token = not current_token.empty()
		
#		printt("AFTER: ", chr, in_string, should_push_token)
		if should_push_token:
			if current_token.empty():
				continue
			var token = current_token.join("")
			
			if in_string:
				token_list.push_back(BoxedString.new(token))
			else:
				token_list.push_back(current_token.join(""))
			
			current_token = PoolStringArray([])
			should_push_token = false
			in_string = false
	
	if not current_token.empty():
		token_list.push_back(current_token.join(""))
	
	return token_list


func parse(token_list: Array) -> Array:
	var parse_array := []
	for i in range(token_list.size()):
		var token = token_list[i]
		match token:
			":":
				i += 1
				var definition := define_word(i, token_list)
				if definition.status == ERROR_OK:
					dictionary[token] = definition.body
					i = definition.i
			"A{":
				pass # define_array

			"D{":
				pass # define_dictionary
			_:
				if token is BoxedString:
					parse_array.push_back(token)

				elif float_regex.search(token):
					parse_array.push_back(float(token))

				elif int_regex.search(token):
					parse_array.push_back(int(token))
				
				elif ClassDB.class_exists(token):
					parse_array.push_back(ClassDB.instance(token))

				else:
					parse_array.push_back(Word.new(token))
	
#	print(JSON.print(parse_array, " "))
	return parse_array


func run(program: String) -> void:
	var token_list := tokenize(program)
	var parse_array := parse(token_list)
	eval(parse_array)


func define_array(i, token_list) -> DefineArrayParseResult:
	return null


func define_dictionary(i, token_list) -> DefineDictParseResult:
	return null


func define_word(i, token_list) -> DefineWordParseResult:
	var body := []
	while token_list[i] != ";":
		body.push_back(token_list[i])
		i += 1
		if not i < token_list.size():
			return DefineWordParseResult.new(ERROR_EOF, body, i)

	i += 1
	return DefineWordParseResult.new(ERROR_OK, body, i)


func eval(parse_array: Array) -> void:
	for i in range(parse_array.size()):
#		print(parse_array[i] is Word)
		if parse_array[i] is Word:
			var word = (parse_array[i] as Word).name
		
			if dictionary.has(word):
				eval(dictionary[word])
		
			elif stack[-1] is Object and stack[-1].has_method(word):
				var obj = stack.pop()
				eval_method(obj, word)
			
			else:
				Builtins.eval(word, stack)

		elif parse_array[i] is BoxedString:
			stack.push_back(parse_array[i].value)

		else:
			stack.push_back(parse_array[i])
	
	emit_signal("stack_changed", stack)


func eval_method(obj: Object, word: String) -> void:
	var methods = obj.get_method_list()
	for method in methods:
		if method.name == word:
			var args := []
			
			for _i in range(method.args.size()):
				args.push_back(stack.pop_back())
			
			args.invert()
			var result = obj.callv(word, args)
			
			if result != null:
				stack.push_back(result)

			break

