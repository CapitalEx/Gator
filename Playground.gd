extends Control

onready var CodeInput = $"%CodeInput"
onready var StackView = $"%StackViewer"

func _ready() -> void:
	CodeInput.connect("gui_input", self, "_on_code_input_gui_input")
	$StackMachine.connect("stack_changed", self, "_on_stack_changed")

func _on_code_input_gui_input(event: InputEvent) -> void:
	if event is InputEventKey:
		if event.is_pressed() and event.scancode == KEY_ENTER and event.shift:
			$StackMachine.run(CodeInput.text)
			CodeInput.text = ""

func _on_stack_changed(stack: Array) -> void:
	for child in StackView.get_children():
		StackView.remove_child(child)
		child.queue_free()
	
	for value in stack:
		var label = Label.new()
		label.text = str(value)
		StackView.add_child(label)
	
